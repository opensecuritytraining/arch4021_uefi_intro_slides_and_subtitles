1
00:00:00,240 --> 00:00:03,919
As we can [have] said

2
00:00:01,920 --> 00:00:05,600
boot manager is responsible for loading

3
00:00:03,919 --> 00:00:07,919
various types of

4
00:00:05,600 --> 00:00:09,840
UEFI images applications,

5
00:00:07,919 --> 00:00:12,000
and according to specification UEFI

6
00:00:09,840 --> 00:00:15,280
recognize the following

7
00:00:12,000 --> 00:00:18,400
binaries or applications. So, first of all

8
00:00:15,280 --> 00:00:21,600
there are UEFI images and UEFI images are

9
00:00:18,400 --> 00:00:23,359
class of files which contain executable

10
00:00:21,600 --> 00:00:24,480
code

11
00:00:23,359 --> 00:00:27,240


12
00:00:24,480 --> 00:00:30,560
the format of that file use subset of

13
00:00:27,240 --> 00:00:33,200
PE32+ specification

14
00:00:30,560 --> 00:00:35,200
and the specification

15
00:00:33,200 --> 00:00:39,040
can be found in internet

16
00:00:35,200 --> 00:00:41,920
and this is open format.

17
00:00:39,040 --> 00:00:46,000
Those files are different by

18
00:00:41,920 --> 00:00:47,840
depending on the subsystem and

19
00:00:46,000 --> 00:00:49,520
machine,

20
00:00:47,840 --> 00:00:51,760
and machine parameter describes the

21
00:00:49,520 --> 00:00:52,960
processor architecture of the executable

22
00:00:51,760 --> 00:00:54,960


23
00:00:52,960 --> 00:00:57,920
so we can have like

24
00:00:54,960 --> 00:01:00,320
various architectures supported

25
00:00:57,920 --> 00:01:02,960
through various

26
00:01:00,320 --> 00:01:05,040
types of files.

27
00:01:02,960 --> 00:01:06,960
And subsystem describes if the

28
00:01:05,040 --> 00:01:09,920
executable is an application, boot

29
00:01:06,960 --> 00:01:13,280
service or runtime service. So,

30
00:01:09,920 --> 00:01:16,080
this is

31
00:01:13,280 --> 00:01:17,439
defined in the .inf file during the

32
00:01:16,080 --> 00:01:19,280
compilation.

33
00:01:17,439 --> 00:01:20,400
There are UEFI applications which are

34
00:01:19,280 --> 00:01:21,520
loaded by

35
00:01:20,400 --> 00:01:24,159


36
00:01:21,520 --> 00:01:26,799
by boot manager.

37
00:01:24,159 --> 00:01:29,200
So some applications, so there's this

38
00:01:26,799 --> 00:01:32,159
this this problem with division of labor

39
00:01:29,200 --> 00:01:33,680
between kernel and and operating systems,

40
00:01:32,159 --> 00:01:34,960
so some application

41
00:01:33,680 --> 00:01:37,360
may perform

42
00:01:34,960 --> 00:01:39,040
ExitBootServices(), so that means they

43
00:01:37,360 --> 00:01:40,720
terminate all the

44
00:01:39,040 --> 00:01:42,880
all the support and pass the

45
00:01:40,720 --> 00:01:46,240
control to something else.

46
00:01:42,880 --> 00:01:49,439
And that's typically the way

47
00:01:46,240 --> 00:01:51,840
how UEFI OS loaders work, 

48
00:01:49,439 --> 00:01:53,600
because those are a UEFI

49
00:01:51,840 --> 00:01:54,399
application that take over platform

50
00:01:53,600 --> 00:01:57,200
control

51
00:01:54,399 --> 00:01:58,560
and pass the control to OS

52
00:01:57,200 --> 00:02:02,560
and either

53
00:01:58,560 --> 00:02:04,880
OS loader or OS kernel at some point and

54
00:02:02,560 --> 00:02:04,880
calls

55
00:02:05,119 --> 00:02:09,759
UEFI ExitBootServices().

56
00:02:07,360 --> 00:02:10,560
And finally there are UEFI drivers which

57
00:02:09,759 --> 00:02:12,080


58
00:02:10,560 --> 00:02:14,000


59
00:02:12,080 --> 00:02:16,080
have two types,

60
00:02:14,000 --> 00:02:17,680
boot service drivers, and runtime service

61
00:02:16,080 --> 00:02:19,920
driver. And

62
00:02:17,680 --> 00:02:23,040
as we explained both service driver

63
00:02:19,920 --> 00:02:25,520
exposed functionality for the OS loader

64
00:02:23,040 --> 00:02:28,080
and runtime service drivers

65
00:02:25,520 --> 00:02:29,840
expose functionality, hardware access

66
00:02:28,080 --> 00:02:32,720
functionality typically,

67
00:02:29,840 --> 00:02:34,800
for operating system.

68
00:02:32,720 --> 00:02:37,599
And

69
00:02:34,800 --> 00:02:39,920
boot manager code understands how to

70
00:02:37,599 --> 00:02:42,640
load those images to those

71
00:02:39,920 --> 00:02:45,120
applications OS loaders drivers

72
00:02:42,640 --> 00:02:47,519
and 

73
00:02:45,120 --> 00:02:50,080
how to provide required configuration data

74
00:02:47,519 --> 00:02:51,440
for those files.

75
00:02:50,080 --> 00:02:53,440


76
00:02:51,440 --> 00:02:56,080
As we mentioned before

77
00:02:53,440 --> 00:02:58,640
those types of files this division

78
00:02:56,080 --> 00:03:02,959
through types of files cause a lot of

79
00:02:58,640 --> 00:03:02,959
confusion because 

80
00:03:03,040 --> 00:03:07,120
since behavior of UEFI application

81
00:03:05,280 --> 00:03:10,720
is not consistent

82
00:03:07,120 --> 00:03:11,840
in context of who calls UEFI

83
00:03:10,720 --> 00:03:13,920
ExitBootServices(),

84
00:03:11,840 --> 00:03:16,239
the responsibility between operating

85
00:03:13,920 --> 00:03:18,000
system and operating system loader is

86
00:03:16,239 --> 00:03:20,480
blurred and

87
00:03:18,000 --> 00:03:23,920
it's not clear and this may cause

88
00:03:20,480 --> 00:03:23,920
various issues.



