1
00:00:00,320 --> 00:00:04,799
What are the protocols?

2
00:00:02,080 --> 00:00:07,200
So, protocols are

3
00:00:04,799 --> 00:00:10,880
just sets of APIs

4
00:00:07,200 --> 00:00:13,519
so we can say that protocols are sets

5
00:00:10,880 --> 00:00:15,519
of or collections of related functions

6
00:00:13,519 --> 00:00:16,640
and data.

7
00:00:15,519 --> 00:00:20,160


8
00:00:16,640 --> 00:00:22,960
Every protocol [has] got a GUID

9
00:00:20,160 --> 00:00:25,680
and some structure of its interface so

10
00:00:22,960 --> 00:00:28,640
some set of functions and some

11
00:00:25,680 --> 00:00:29,610
some data information or some structures

12
00:00:28,640 --> 00:00:30,880
and

13
00:00:29,610 --> 00:00:33,680


14
00:00:30,880 --> 00:00:36,559
and those protocols have associated

15
00:00:33,680 --> 00:00:37,840
API which is called protocol

16
00:00:36,559 --> 00:00:39,520
services.

17
00:00:37,840 --> 00:00:42,719
And

18
00:00:39,520 --> 00:00:45,280
those those protocols can be used only

19
00:00:42,719 --> 00:00:46,399
at boot time.

20
00:00:45,280 --> 00:00:48,160
So in

21
00:00:46,399 --> 00:00:50,800
UEFI 2.9

22
00:00:48,160 --> 00:00:53,120
we have

23
00:00:50,800 --> 00:00:56,320
over 60 protocols and for example some

24
00:00:53,120 --> 00:00:59,120
protocols are like PCI IO protocol

25
00:00:56,320 --> 00:01:01,600
serial IO protocol and other IO

26
00:00:59,120 --> 00:01:03,440
protocol abstracting

27
00:01:01,600 --> 00:01:04,799
IO configuration

28
00:01:03,440 --> 00:01:07,439
using given

29
00:01:04,799 --> 00:01:09,439
standard

30
00:01:07,439 --> 00:01:12,960
bus.

31
00:01:09,439 --> 00:01:15,119
So, on the diagram we see that

32
00:01:12,960 --> 00:01:17,119
the there is some handle

33
00:01:15,119 --> 00:01:18,479
and

34
00:01:17,119 --> 00:01:21,119


35
00:01:18,479 --> 00:01:23,119
the handle contain-

36
00:01:21,119 --> 00:01:25,680
on the handle we install some protocol

37
00:01:23,119 --> 00:01:28,240
interfaces. So those protocol interfaces are-

38
00:01:25,680 --> 00:01:30,799
there can be multiple protocol

39
00:01:28,240 --> 00:01:32,799
interfaces installed on a given handle.

40
00:01:30,799 --> 00:01:35,119
Each

41
00:01:32,799 --> 00:01:38,320
protocol interface got a

42
00:01:35,119 --> 00:01:39,680
function pointer

43
00:01:38,320 --> 00:01:42,960
which points to

44
00:01:39,680 --> 00:01:44,799
some other driver which then have some

45
00:01:42,960 --> 00:01:46,000
implementation of some specific

46
00:01:44,799 --> 00:01:47,759
functions.

47
00:01:46,000 --> 00:01:50,399
So, we can say that

48
00:01:47,759 --> 00:01:52,880
the protocol

49
00:01:50,399 --> 00:01:55,520
the protocol interface

50
00:01:52,880 --> 00:01:57,200
can point to multiple-so for example

51
00:01:55,520 --> 00:01:58,960
there may be

52
00:01:57,200 --> 00:02:01,920
reuse of

53
00:01:58,960 --> 00:02:05,439
functions defined in EFI drivers by

54
00:02:01,920 --> 00:02:05,439
multiple protocol interfaces.

55
00:02:05,680 --> 00:02:10,640
And the drivers and the

56
00:02:07,840 --> 00:02:11,680
functions and interfaces can be then

57
00:02:10,640 --> 00:02:14,959
nested,

58
00:02:11,680 --> 00:02:16,560
so multiple levels of drivers can be

59
00:02:14,959 --> 00:02:19,680
used to

60
00:02:16,560 --> 00:02:19,680
execute given action.



