1
00:00:00,640 --> 00:00:08,160
So, first of all EFI was less limited

2
00:00:04,640 --> 00:00:10,559
without a grand vision of unification.

3
00:00:08,160 --> 00:00:13,519
So, they mostly solved the

4
00:00:10,559 --> 00:00:15,040
key problems of early firmware. First

5
00:00:13,519 --> 00:00:16,780
of all was

6
00:00:15,040 --> 00:00:17,920
support for large disks so

7
00:00:16,780 --> 00:00:20,640
before EFI

8
00:00:17,920 --> 00:00:21,920
support for bigger than 2TB

9
00:00:20,640 --> 00:00:24,160


10
00:00:21,920 --> 00:00:25,039
hard disk was not

11
00:00:24,160 --> 00:00:28,160
easy.

12
00:00:25,039 --> 00:00:28,960
And with EFI it was implemented and

13
00:00:28,160 --> 00:00:32,320
the

14
00:00:28,960 --> 00:00:36,320
storage capabilities started to be

15
00:00:32,320 --> 00:00:38,320
unlimited at least for now.

16
00:00:36,320 --> 00:00:40,719
Architecture independence

17
00:00:38,320 --> 00:00:42,480
so you know like

18
00:00:40,719 --> 00:00:46,239
moving to other

19
00:00:42,480 --> 00:00:49,120
architectures like ARM, RISC-V was not

20
00:00:46,239 --> 00:00:51,440
at that point but EFI introduced that

21
00:00:49,120 --> 00:00:55,360
capability also the

22
00:00:51,440 --> 00:00:56,399
question about 32-bit versus 64-bit

23
00:00:55,360 --> 00:00:59,280
also

24
00:00:56,399 --> 00:01:00,480
had to be resolved and that was the

25
00:00:59,280 --> 00:01:03,199
feature of

26
00:01:00,480 --> 00:01:05,280
EFI. Modular design and Backward

27
00:01:03,199 --> 00:01:07,520
compatibility. Backward compatibility was

28
00:01:05,280 --> 00:01:10,240
introduced through a

29
00:01:07,520 --> 00:01:11,680
compatibility support module

30
00:01:10,240 --> 00:01:14,400
and

31
00:01:11,680 --> 00:01:15,520
what are the key UEFI features. So

32
00:01:14,400 --> 00:01:17,920
definitely

33
00:01:15,520 --> 00:01:21,040
unified interfaces for programming

34
00:01:17,920 --> 00:01:23,360
and services which could be run

35
00:01:21,040 --> 00:01:25,680
runtime and and during boot process so

36
00:01:23,360 --> 00:01:28,080
both services runtime services which we

37
00:01:25,680 --> 00:01:30,320
will discuss further.

38
00:01:28,080 --> 00:01:33,119
Then

39
00:01:30,320 --> 00:01:36,159
there is open standard with

40
00:01:33,119 --> 00:01:37,439
modern mostly C implementation but there

41
00:01:36,159 --> 00:01:38,880
are also

42
00:01:37,439 --> 00:01:40,720
...

43
00:01:38,880 --> 00:01:43,600
bindings for other languages like

44
00:01:40,720 --> 00:01:46,479
MicroPython or Rust. So Rust we can even

45
00:01:43,600 --> 00:01:49,200
write drivers or applications in Rust. 

46
00:01:46,479 --> 00:01:50,159
There's support for modern buses

47
00:01:49,200 --> 00:01:51,680


48
00:01:50,159 --> 00:01:54,799
like PCI,

49
00:01:51,680 --> 00:01:58,320
PCIe, SCSI, USB, ACPI.

50
00:01:54,799 --> 00:02:02,240
So ACPI is like modern 

51
00:01:58,320 --> 00:02:04,159
spec and standard, not bus.

52
00:02:02,240 --> 00:02:07,280
But there are also other standards

53
00:02:04,159 --> 00:02:09,599
supported by UEFI so

54
00:02:07,280 --> 00:02:11,440
even some networking one.

55
00:02:09,599 --> 00:02:12,879
A lot of security features secure boot

56
00:02:11,440 --> 00:02:14,959
driver signing

57
00:02:12,879 --> 00:02:16,720
and verification of that

58
00:02:14,959 --> 00:02:17,920
signature during this

59
00:02:16,720 --> 00:02:19,120
boot process.

60
00:02:17,920 --> 00:02:22,400


61
00:02:19,120 --> 00:02:24,800
Crypto functions and key management

62
00:02:22,400 --> 00:02:28,879
also user interface which right now can

63
00:02:24,800 --> 00:02:34,160
be very sophisticated including some

64
00:02:28,879 --> 00:02:36,480
nice visual effects and

65
00:02:34,160 --> 00:02:37,599
be even like fully blown operating

66
00:02:36,480 --> 00:02:38,640
system.

67
00:02:37,599 --> 00:02:42,480


68
00:02:38,640 --> 00:02:44,480
Then modern runtime services, which is

69
00:02:42,480 --> 00:02:46,959
a capability delivered for the operating

70
00:02:44,480 --> 00:02:50,640
system for various reasons for example

71
00:02:46,959 --> 00:02:51,920
obtaining memory map, obtaining

72
00:02:50,640 --> 00:02:56,080
time or

73
00:02:51,920 --> 00:02:56,080
various other features.

74
00:02:56,160 --> 00:03:00,879
Non-volatile OS-accessible configuration

75
00:02:58,720 --> 00:03:01,920
variables this is also something

76
00:03:00,879 --> 00:03:04,560
introduced

77
00:03:01,920 --> 00:03:06,560
in UEFI we will talk about the variables

78
00:03:04,560 --> 00:03:09,280
in other lectures,

79
00:03:06,560 --> 00:03:10,080
and built-in shell which is very MS-DOS

80
00:03:09,280 --> 00:03:11,690
like

81
00:03:10,080 --> 00:03:12,800


82
00:03:11,690 --> 00:03:15,599


83
00:03:12,800 --> 00:03:16,840
execution environment.

84
00:03:15,599 --> 00:03:24,319
So,

85
00:03:16,840 --> 00:03:24,319
EFI worked in protected mode which provided

86
00:03:24,640 --> 00:03:28,720
long and virtual address space for

87
00:03:27,360 --> 00:03:29,599
applications.

88
00:03:28,720 --> 00:03:32,959


89
00:03:29,599 --> 00:03:35,040
So, it resolved the problem

90
00:03:32,959 --> 00:03:37,599
of addressability

91
00:03:35,040 --> 00:03:38,959
which was in legacy BIOS.

92
00:03:37,599 --> 00:03:43,280


93
00:03:38,959 --> 00:03:44,159
So, UEFI is highly abstracted and

94
00:03:43,280 --> 00:03:47,360


95
00:03:44,159 --> 00:03:51,599
quite complex firmware architecture and

96
00:03:47,360 --> 00:03:53,920
the UEFI spec has more than 2000

97
00:03:51,599 --> 00:03:56,080
pages, PI spec got

98
00:03:53,920 --> 00:03:56,959
more than one thousand pages.

99
00:03:56,080 --> 00:04:00,239


100
00:03:56,959 --> 00:04:01,599
The UEFI API differs a lot from the

101
00:04:00,239 --> 00:04:04,480
legacy,

102
00:04:01,599 --> 00:04:05,599
BIOS interrupts.

103
00:04:04,480 --> 00:04:07,840
But,

104
00:04:05,599 --> 00:04:08,879
maybe from that point it's even

105
00:04:07,840 --> 00:04:11,840
better

106
00:04:08,879 --> 00:04:11,840
because it provides

107
00:04:12,000 --> 00:04:16,239
open specification and it's easier

108
00:04:14,959 --> 00:04:18,239
to modify.

109
00:04:16,239 --> 00:04:21,680
It most probably more

110
00:04:18,239 --> 00:04:24,160
secure and faster

111
00:04:21,680 --> 00:04:26,479
to use, faster to

112
00:04:24,160 --> 00:04:28,720
extend.

113
00:04:26,479 --> 00:04:30,800
So, there were not many BIOS

114
00:04:28,720 --> 00:04:35,600
problems that EFI already solved and

115
00:04:30,800 --> 00:04:35,600
then UEFI features improved.



