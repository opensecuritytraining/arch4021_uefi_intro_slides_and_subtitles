1
00:00:00,160 --> 00:00:07,600
How architecture overview look

2
00:00:03,360 --> 00:00:09,840
like? So, this is a picture

3
00:00:07,600 --> 00:00:12,880
about which there are jokes that

4
00:00:09,840 --> 00:00:15,360
there is no UEFI presentation without

5
00:00:12,880 --> 00:00:18,320
this picture, so here it is.

6
00:00:15,360 --> 00:00:21,119
Let's start from the left to right,

7
00:00:18,320 --> 00:00:24,800
so first of all columns. So, we on the

8
00:00:21,119 --> 00:00:27,359
X axis we have timeline of

9
00:00:24,800 --> 00:00:29,199
platform

10
00:00:27,359 --> 00:00:30,640
runtime process or boot process, 

11
00:00:29,199 --> 00:00:33,440
runtime process,

12
00:00:30,640 --> 00:00:34,640
and and we starting on the left side

13
00:00:33,440 --> 00:00:37,280
with some

14
00:00:34,640 --> 00:00:39,360
very early security phase which

15
00:00:37,280 --> 00:00:41,840
should start after power on but there

16
00:00:39,360 --> 00:00:44,160
are some nitty gritty details related to

17
00:00:41,840 --> 00:00:46,719
how modern platforms boot. So, this is

18
00:00:44,160 --> 00:00:48,719
maybe a little bit

19
00:00:46,719 --> 00:00:53,520
different right now and all those

20
00:00:48,719 --> 00:00:54,480
columns like SEC, PEI, DXE, BDS, TSL, RT,

21
00:00:53,520 --> 00:00:56,320
AL,

22
00:00:54,480 --> 00:00:59,199
define the

23
00:00:56,320 --> 00:01:01,840
stages in the

24
00:00:59,199 --> 00:01:05,519
in the PI specification

25
00:01:01,840 --> 00:01:09,200
and some of the components here

26
00:01:05,519 --> 00:01:12,000
UEFI are described in the

27
00:01:09,200 --> 00:01:14,720
UEFI specification. Mostly this is about

28
00:01:12,000 --> 00:01:15,520
this bold

29
00:01:14,720 --> 00:01:18,479
line

30
00:01:15,520 --> 00:01:21,280
which is marked with UEFI interface so

31
00:01:18,479 --> 00:01:24,720
that is what UEFI specification

32
00:01:21,280 --> 00:01:26,640
talking about. The interface exposed

33
00:01:24,720 --> 00:01:29,520
at this point

34
00:01:26,640 --> 00:01:32,720
for boot manager and for

35
00:01:29,520 --> 00:01:34,640
BDS boot stage to

36
00:01:32,720 --> 00:01:35,759
finally load

37
00:01:34,640 --> 00:01:38,159
some

38
00:01:35,759 --> 00:01:39,680
application, OS-absent application, for

39
00:01:38,159 --> 00:01:41,600
example like

40
00:01:39,680 --> 00:01:43,360
mem test or some

41
00:01:41,600 --> 00:01:45,119
some bare metal

42
00:01:43,360 --> 00:01:46,880
application which running

43
00:01:45,119 --> 00:01:50,159
without

44
00:01:46,880 --> 00:01:52,640
operating system or

45
00:01:50,159 --> 00:01:56,159
run OS boot loader which essentially

46
00:01:52,640 --> 00:01:58,000
will kick the operating system kernel

47
00:01:56,159 --> 00:02:00,240
and move on with the

48
00:01:58,000 --> 00:02:02,000
platform lifetime.

49
00:02:00,240 --> 00:02:02,799
We will talk about all these components

50
00:02:02,000 --> 00:02:05,520
in

51
00:02:02,799 --> 00:02:05,520
future slides.

52
00:02:05,759 --> 00:02:11,200
So, as we 

53
00:02:08,800 --> 00:02:14,959
saw on previous slide

54
00:02:11,200 --> 00:02:18,000
UEFI covers only a very small part of

55
00:02:14,959 --> 00:02:19,440
boot firmware so just this interface the

56
00:02:18,000 --> 00:02:21,360
PI cover

57
00:02:19,440 --> 00:02:23,520
much wider. So hardware initialization is

58
00:02:21,360 --> 00:02:25,920
the subject, subject of the platform

59
00:02:23,520 --> 00:02:28,800
initialization specification and the most

60
00:02:25,920 --> 00:02:30,560
important important phase

61
00:02:28,800 --> 00:02:32,959
for PI are

62
00:02:30,560 --> 00:02:34,560
SEC. So, the

63
00:02:32,959 --> 00:02:36,239
rectangle first

64
00:02:34,560 --> 00:02:37,599


65
00:02:36,239 --> 00:02:40,239
column

66
00:02:37,599 --> 00:02:41,280
then PEI and those two are quite

67
00:02:40,239 --> 00:02:42,400
extensively

68
00:02:41,280 --> 00:02:43,599
described.

69
00:02:42,400 --> 00:02:45,599
But,

70
00:02:43,599 --> 00:02:48,080
implementation of those phases is

71
00:02:45,599 --> 00:02:49,519
typically

72
00:02:48,080 --> 00:02:52,959
up to the

73
00:02:49,519 --> 00:02:56,400
IBVs Independent BIOS Vendors and

74
00:02:52,959 --> 00:02:56,400
platform designers.

75
00:02:56,480 --> 00:03:02,560
So, UEFI talks only about the interface

76
00:03:01,120 --> 00:03:04,239
where

77
00:03:02,560 --> 00:03:06,879
drivers

78
00:03:04,239 --> 00:03:09,840
loaded during the boot process expose

79
00:03:06,879 --> 00:03:12,800
some information to

80
00:03:09,840 --> 00:03:14,239
bootloader, so it can

81
00:03:12,800 --> 00:03:15,200
leverage those

82
00:03:14,239 --> 00:03:18,480


83
00:03:15,200 --> 00:03:21,599
drivers those interfaces exposed through

84
00:03:18,480 --> 00:03:24,319
those drivers to access

85
00:03:21,599 --> 00:03:27,040
some boot device in the boot device

86
00:03:24,319 --> 00:03:28,640
selection phase.

87
00:03:27,040 --> 00:03:29,920
There is also interface for

88
00:03:28,640 --> 00:03:31,360
communication

89
00:03:29,920 --> 00:03:33,440
with the OS

90
00:03:31,360 --> 00:03:35,920
and so for example operating system may

91
00:03:33,440 --> 00:03:38,400
want to change boot order may want to do

92
00:03:35,920 --> 00:03:40,879
the firmware update through capsule

93
00:03:38,400 --> 00:03:43,200
update and that needs some

94
00:03:40,879 --> 00:03:46,400
runtime services, which are used by

95
00:03:43,200 --> 00:03:49,280
operating system. This is also defined by

96
00:03:46,400 --> 00:03:51,120
UEFI specification.

97
00:03:49,280 --> 00:03:52,720


98
00:03:51,120 --> 00:03:54,720
So

99
00:03:52,720 --> 00:03:56,000
if we talk about various stages of the

100
00:03:54,720 --> 00:03:58,239
boot process

101
00:03:56,000 --> 00:04:00,480
in UEFI, only the

102
00:03:58,239 --> 00:04:03,200
driver execution environment so DXE ("Dixie")

103
00:04:00,480 --> 00:04:07,280
phase has detailed description in the

104
00:04:03,200 --> 00:04:09,120
UEFI spec. The SEC and PEI are

105
00:04:07,280 --> 00:04:11,760
described in the

106
00:04:09,120 --> 00:04:13,840
PI specification but not

107
00:04:11,760 --> 00:04:16,799


108
00:04:13,840 --> 00:04:19,359
despite very extensive way 

109
00:04:16,799 --> 00:04:22,000
there is a lot of flexibility for the

110
00:04:19,359 --> 00:04:24,240
designers and IBVs to implement that

111
00:04:22,000 --> 00:04:24,240
phase.



