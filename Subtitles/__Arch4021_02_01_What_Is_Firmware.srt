1
00:00:00,160 --> 00:00:05,200
Let's start with firmware fundamentals.

2
00:00:03,760 --> 00:00:07,600
So, what is

3
00:00:05,200 --> 00:00:09,440
firmware? Some people say that firmware

4
00:00:07,600 --> 00:00:10,880
is a software that is hard to get,

5
00:00:09,440 --> 00:00:13,360
there's some true in that because

6
00:00:10,880 --> 00:00:15,440
typically firmware is built into some

7
00:00:13,360 --> 00:00:17,760
special dedicated storage

8
00:00:15,440 --> 00:00:18,560
which is hard to

9
00:00:17,760 --> 00:00:20,560
get

10
00:00:18,560 --> 00:00:22,000
how to interact with like you need some

11
00:00:20,560 --> 00:00:24,640
additional

12
00:00:22,000 --> 00:00:26,640
tools to interact with it.

13
00:00:24,640 --> 00:00:29,119
Some people say it is software

14
00:00:26,640 --> 00:00:31,519
considered by users as a part of the

15
00:00:29,119 --> 00:00:33,920
hardware, this is very true in

16
00:00:31,519 --> 00:00:36,480
the modern computing environment because

17
00:00:33,920 --> 00:00:39,440
when you're buying your laptop or when

18
00:00:36,480 --> 00:00:41,760
you're buying your PC you expect it will

19
00:00:39,440 --> 00:00:44,719
boot and then you can run your operating

20
00:00:41,760 --> 00:00:47,600
system, so that means the

21
00:00:44,719 --> 00:00:49,920
firmware the BIOS firmware is inside the

22
00:00:47,600 --> 00:00:52,399
device and this integral part of the

23
00:00:49,920 --> 00:00:54,960
device and that's the expectation of

24
00:00:52,399 --> 00:00:56,320
users. Others say that this is lowest

25
00:00:54,960 --> 00:00:58,000
level software

26
00:00:56,320 --> 00:01:00,960
which provides hardware initialization

27
00:00:58,000 --> 00:01:03,760
this is more close to

28
00:01:00,960 --> 00:01:05,760
our point of view because

29
00:01:03,760 --> 00:01:08,400
we're working with firmware to really

30
00:01:05,760 --> 00:01:11,360
initialize hardware so this can be

31
00:01:08,400 --> 00:01:13,920
leveraged for security purposes and

32
00:01:11,360 --> 00:01:17,040
leveraged by operating system

33
00:01:13,920 --> 00:01:19,040
to maximize performance to run some

34
00:01:17,040 --> 00:01:21,600
applications.

35
00:01:19,040 --> 00:01:23,119
Some people say

36
00:01:21,600 --> 00:01:25,600
the firmware is

37
00:01:23,119 --> 00:01:27,600
just the hardware initialization

38
00:01:25,600 --> 00:01:32,240
software which allows to

39
00:01:27,600 --> 00:01:34,240
make it more flexible and compatible. So

40
00:01:32,240 --> 00:01:36,400
thanks to firmware, we can run multiple

41
00:01:34,240 --> 00:01:38,560
applications of applications of choices

42
00:01:36,400 --> 00:01:41,040
as in previous case.

43
00:01:38,560 --> 00:01:42,799
But, why operating system not doing that

44
00:01:41,040 --> 00:01:44,960
all that initialization?

45
00:01:42,799 --> 00:01:47,119


46
00:01:44,960 --> 00:01:48,640
There is a lot of philosophical

47
00:01:47,119 --> 00:01:50,720
discussion about that,

48
00:01:48,640 --> 00:01:53,520
there are some people from

49
00:01:50,720 --> 00:01:56,159
Linux boot community and previous

50
00:01:53,520 --> 00:01:58,640
Linux BIOS coreboot environment where

51
00:01:56,159 --> 00:02:01,520
they would like to put Linux in

52
00:01:58,640 --> 00:02:03,759
the firmware and to not repeat

53
00:02:01,520 --> 00:02:05,040
writing the drivers twice for

54
00:02:03,759 --> 00:02:05,920
once for

55
00:02:05,040 --> 00:02:07,439


56
00:02:05,920 --> 00:02:09,679
initialization of the platform and

57
00:02:07,439 --> 00:02:11,840
second time for the operating system.

58
00:02:09,679 --> 00:02:13,440
So, do not repeat yourself rule.

59
00:02:11,840 --> 00:02:16,720


60
00:02:13,440 --> 00:02:19,440
When we look at this

61
00:02:16,720 --> 00:02:21,920
effort at Linux BIOS then coreboot and

62
00:02:19,440 --> 00:02:24,160
after that Linux boot, we see that this

63
00:02:21,920 --> 00:02:25,280
is easier to say and harder

64
00:02:24,160 --> 00:02:27,599
to do

65
00:02:25,280 --> 00:02:30,080
because there is still a lot of

66
00:02:27,599 --> 00:02:32,080
obstacles to achieve that state.

67
00:02:30,080 --> 00:02:34,400
There are also some critical parts that

68
00:02:32,080 --> 00:02:37,040
shouldn't be done by operating system, so

69
00:02:34,400 --> 00:02:41,200
operating system is definitely big

70
00:02:37,040 --> 00:02:41,200
code base which is hard to

71
00:02:41,840 --> 00:02:46,720
verify, hard to write hard to maintain

72
00:02:44,879 --> 00:02:48,080
and firmware should definitely do a

73
00:02:46,720 --> 00:02:50,000
more specific thing and should be

74
00:02:48,080 --> 00:02:51,680
smaller.

75
00:02:50,000 --> 00:02:52,959
So, this is one side of the what is

76
00:02:51,680 --> 00:02:54,720
firmware.

77
00:02:52,959 --> 00:02:56,560
The other side of the

78
00:02:54,720 --> 00:02:59,440
of the firmware is

79
00:02:56,560 --> 00:03:00,480
where it is, how this is stored and

80
00:02:59,440 --> 00:03:04,000
this is

81
00:03:00,480 --> 00:03:05,599
how it differs from other software. So

82
00:03:04,000 --> 00:03:07,920
typically firmware is stored in a

83
00:03:05,599 --> 00:03:09,200
separate chip on the main board

84
00:03:07,920 --> 00:03:13,120
typically on

85
00:03:09,200 --> 00:03:15,360
SPI chip and access to firmware

86
00:03:13,120 --> 00:03:16,080
code is restricted

87
00:03:15,360 --> 00:03:18,959


88
00:03:16,080 --> 00:03:21,840
and limited, 

89
00:03:18,959 --> 00:03:23,760
so the attacker who will

90
00:03:21,840 --> 00:03:24,640
like to make

91
00:03:23,760 --> 00:03:27,360
a

92
00:03:24,640 --> 00:03:29,599
persistent threat on the platform

93
00:03:27,360 --> 00:03:32,319
will not get easy access to

94
00:03:29,599 --> 00:03:34,720
this storage, they will

95
00:03:32,319 --> 00:03:36,080
not have easy access to modify it.

96
00:03:34,720 --> 00:03:38,959


97
00:03:36,080 --> 00:03:41,519
For security and intellectual property

98
00:03:38,959 --> 00:03:42,400
protection reasons, some platforms

99
00:03:41,519 --> 00:03:45,680
may

100
00:03:42,400 --> 00:03:48,480
use some built-in firmware

101
00:03:45,680 --> 00:03:50,159
firmware built into processor we

102
00:03:48,480 --> 00:03:52,000
call it boot ROM.

103
00:03:50,159 --> 00:03:54,799


104
00:03:52,000 --> 00:03:58,159
ROM is a read-only storage which is

105
00:03:54,799 --> 00:04:01,599
internally to the SOC,

106
00:03:58,159 --> 00:04:03,760
and it execute some most critical

107
00:04:01,599 --> 00:04:06,239
firmware and then pass the control to

108
00:04:03,760 --> 00:04:07,920
the further stages of the boot.

109
00:04:06,239 --> 00:04:09,840


110
00:04:07,920 --> 00:04:11,439
Some people also say that the

111
00:04:09,840 --> 00:04:14,720
firmware is a program that just the

112
00:04:11,439 --> 00:04:16,160
loads operating system and try to call

113
00:04:14,720 --> 00:04:18,479
firmware

114
00:04:16,160 --> 00:04:20,320
bootloader, so this is

115
00:04:18,479 --> 00:04:23,360
probably opinion from some silicon

116
00:04:20,320 --> 00:04:25,650
vendors or maybe also from some

117
00:04:23,360 --> 00:04:26,720
members of UEFI forum.

118
00:04:25,650 --> 00:04:28,880


119
00:04:26,720 --> 00:04:31,360
And, what about the memory of the

120
00:04:28,880 --> 00:04:33,360
firmware? It is typically freed after

121
00:04:31,360 --> 00:04:36,560
passing control to boot loader, so

122
00:04:33,360 --> 00:04:39,040
whatever memory is used by the software

123
00:04:36,560 --> 00:04:40,560
which initialize our hardware

124
00:04:39,040 --> 00:04:44,639
is then

125
00:04:40,560 --> 00:04:46,800
revealed so it is not blocked by the

126
00:04:44,639 --> 00:04:49,040
firware components. Although, there

127
00:04:46,800 --> 00:04:51,680
are some corner cases to that and we

128
00:04:49,040 --> 00:04:54,320
might discuss that later.

129
00:04:51,680 --> 00:04:56,720
To boot operating system we

130
00:04:54,320 --> 00:04:58,960
have to enable at least

131
00:04:56,720 --> 00:05:02,000
some boot devices and provide some

132
00:04:58,960 --> 00:05:04,960
support for file system, because without

133
00:05:02,000 --> 00:05:07,120
that there is problem of loading the

134
00:05:04,960 --> 00:05:12,000
bootloader from some file system like

135
00:05:07,120 --> 00:05:15,039
FAT32, or NTFS, or EXT4.

136
00:05:12,000 --> 00:05:17,360
In practice we have to do a

137
00:05:15,039 --> 00:05:18,800
lot of

138
00:05:17,360 --> 00:05:22,479
things to

139
00:05:18,800 --> 00:05:24,160
initialize the boot devices and then

140
00:05:22,479 --> 00:05:26,960
also

141
00:05:24,160 --> 00:05:28,800
initialize file system drivers.

142
00:05:26,960 --> 00:05:30,479
And usually for compatibility and

143
00:05:28,800 --> 00:05:32,960
performing 

144
00:05:30,479 --> 00:05:34,400
performing the most critical

145
00:05:32,960 --> 00:05:37,120
operations,

146
00:05:34,400 --> 00:05:39,520
we want to have

147
00:05:37,120 --> 00:05:42,479
that code as close to hardware as

148
00:05:39,520 --> 00:05:43,919
possible and run as early as possible. So,

149
00:05:42,479 --> 00:05:46,320
other way to explain the reasoning

150
00:05:43,919 --> 00:05:49,280
behind firmware existence is also

151
00:05:46,320 --> 00:05:50,639
the path that we get through during

152
00:05:49,280 --> 00:05:52,160
the boot process.

153
00:05:50,639 --> 00:05:54,160
It is

154
00:05:52,160 --> 00:05:55,680
from very hardware specific and

155
00:05:54,160 --> 00:05:58,240
constrained environments at the

156
00:05:55,680 --> 00:05:59,600
beginning, which can you which you

157
00:05:58,240 --> 00:06:01,039
can learn in

158
00:05:59,600 --> 00:06:03,840


159
00:06:01,039 --> 00:06:07,120
architecture 4001 for example.

160
00:06:03,840 --> 00:06:09,440
So, this is real mode reset vector area

161
00:06:07,120 --> 00:06:10,960
and then from that mode we are

162
00:06:09,440 --> 00:06:13,840
progressing to more and more

163
00:06:10,960 --> 00:06:17,280
sophisticated environment where finally

164
00:06:13,840 --> 00:06:18,880
we can run fully featured

165
00:06:17,280 --> 00:06:21,600
operating system with a lot of

166
00:06:18,880 --> 00:06:24,560
sophisticated features and leverage all

167
00:06:21,600 --> 00:06:27,120
this complex hardware that we have

168
00:06:24,560 --> 00:06:27,120
behind.



